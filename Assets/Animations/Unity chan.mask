%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Unity chan
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: SpriteMeshes
    m_Weight: 1
  - m_Path: SpriteMeshes/hair_2
    m_Weight: 1
  - m_Path: SpriteMeshes/hair_1
    m_Weight: 1
  - m_Path: SpriteMeshes/ribbon_3
    m_Weight: 1
  - m_Path: SpriteMeshes/ribbon_2
    m_Weight: 1
  - m_Path: SpriteMeshes/sleeve_back_2
    m_Weight: 1
  - m_Path: SpriteMeshes/arm_2
    m_Weight: 1
  - m_Path: SpriteMeshes/sleeve_2
    m_Weight: 1
  - m_Path: SpriteMeshes/leg_2
    m_Weight: 1
  - m_Path: SpriteMeshes/leg_1
    m_Weight: 1
  - m_Path: SpriteMeshes/neck
    m_Weight: 1
  - m_Path: SpriteMeshes/hips
    m_Weight: 1
  - m_Path: SpriteMeshes/dress
    m_Weight: 1
  - m_Path: SpriteMeshes/eye_ball_1
    m_Weight: 1
  - m_Path: SpriteMeshes/eye_ball_2
    m_Weight: 1
  - m_Path: SpriteMeshes/eye_1
    m_Weight: 1
  - m_Path: SpriteMeshes/eye_2
    m_Weight: 1
  - m_Path: SpriteMeshes/face
    m_Weight: 1
  - m_Path: SpriteMeshes/ribbon
    m_Weight: 1
  - m_Path: SpriteMeshes/eye_lash_1
    m_Weight: 1
  - m_Path: SpriteMeshes/eye_lash_2
    m_Weight: 1
  - m_Path: SpriteMeshes/hair_4
    m_Weight: 1
  - m_Path: SpriteMeshes/hair_3
    m_Weight: 1
  - m_Path: SpriteMeshes/sleeve_1_back
    m_Weight: 1
  - m_Path: SpriteMeshes/arm_1
    m_Weight: 1
  - m_Path: SpriteMeshes/sleeve_1
    m_Weight: 1
  - m_Path: SpriteMeshes/_blinkRight
    m_Weight: 1
  - m_Path: SpriteMeshes/_blinkLeft
    m_Weight: 1
  - m_Path: SpriteMeshes/eye_lash_1Blink
    m_Weight: 1
  - m_Path: SpriteMeshes/eye_lash_2Blink
    m_Weight: 1
  - m_Path: Hips
    m_Weight: 1
  - m_Path: Hips/Spine
    m_Weight: 1
  - m_Path: Hips/Spine/Neck
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/right_eye
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/left_eye
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/Right_hair_tail_1
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/Right_hair_tail_1/Right_hair_tail_2
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/Right_hair_tail_1/Right_hair_tail_2/Right_hair_tail_3
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/Left_hair_tail_1
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/Left_hair_tail_1/Left_hair_tail_2
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/Left_hair_tail_1/Left_hair_tail_2/Left_hair_tail_3
    m_Weight: 1
  - m_Path: Hips/Spine/Neck/Head/Right_hair_1
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Right_hair_1/Right_hair_2
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Right_hair_1/Right_hair_2/Right_hair_5
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Right_hair_1/Right_hair_2/Right_hair_5/Right_hair_6
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Right_hair_1/Right_hair_2/Right_hair_5/Right_hair_6/Right_hair_7
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Right_hair_1/Right_hair_2/Right_hair_5/Right_hair_6/Right_hair_8
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Right_hair_1/Right_hair_2/Right_hair_3
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Right_hair_1/Right_hair_2/Right_hair_3/Right_hair_4
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Left_hair_1
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Left_hair_1/Left_hair_2
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Left_hair_1/Left_hair_2/Left_hair_3
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Left_hair_1/Left_hair_2/Left_hair_3/Left_hair_4
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Left_hair_1/Left_hair_2/Left_hair_3/Left_hair_4/Left_hair_5
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Left_hair_1/Left_hair_2/Left_hair_3/Left_hair_6
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Left_hair_1/Left_hair_2/Left_hair_3/Left_hair_6/Left_hair_7
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Left_hair_1/Left_hair_2/Left_hair_3/Left_hair_6/Left_hair_8
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Middle_hair_1
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Middle_hair_1/Middle_hair_2
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Middle_hair_1/Middle_hair_2/Middle_hair_3
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Middle_hair_1/Middle_hair_2/Middle_hair_3/Middle_hair_4
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Middle_hair_1/Middle_hair_2/Middle_hair_3/Middle_hair_4/Middle_hair_5
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_right_1
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_right_1/Ribbon_right_2
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_right_1/Ribbon_right_2/Ribbon_right_3
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_right_1/Ribbon_right_2/Ribbon_right_3/Ribbon_right_4
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_right_1/Ribbon_right_2/Ribbon_right_3/Ribbon_right_4/Ribbon_right_5
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_left_1
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_left_1/Ribbon_left_2
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_left_1/Ribbon_left_2/Ribbon_left_3
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_left_1/Ribbon_left_2/Ribbon_left_3/Ribbon_left_4
    m_Weight: 0
  - m_Path: Hips/Spine/Neck/Head/Ribbon_left_1/Ribbon_left_2/Ribbon_left_3/Ribbon_left_4/Ribbon_left_5
    m_Weight: 0
  - m_Path: Hips/Spine/Right Arm
    m_Weight: 1
  - m_Path: Hips/Spine/Right Arm/Right Forearm
    m_Weight: 1
  - m_Path: Hips/Spine/Right Arm/Right Forearm/Right Hand
    m_Weight: 1
  - m_Path: Hips/Spine/Right Arm/Right Forearm Clothes
    m_Weight: 1
  - m_Path: Hips/Spine/Left Arm
    m_Weight: 1
  - m_Path: Hips/Spine/Left Arm/Left Forearm
    m_Weight: 1
  - m_Path: Hips/Spine/Left Arm/Left Forearm/Left Hand
    m_Weight: 1
  - m_Path: Hips/Spine/Left Arm/Left Forearm Clothes
    m_Weight: 1
  - m_Path: Hips/Right Leg
    m_Weight: 0
  - m_Path: Hips/Right Leg/Right Calf
    m_Weight: 0
  - m_Path: Hips/Right Leg/Right Calf/Right Foot
    m_Weight: 0
  - m_Path: Hips/Left Leg
    m_Weight: 0
  - m_Path: Hips/Left Leg/Left Calf
    m_Weight: 0
  - m_Path: Hips/Left Leg/Left Calf/Left Foot
    m_Weight: 0
  - m_Path: Hips/Shirt_1
    m_Weight: 1
  - m_Path: Hips/Shirt_2
    m_Weight: 1
  - m_Path: IKs
    m_Weight: 1
  - m_Path: IKs/Right arm Ik Limb
    m_Weight: 1
  - m_Path: IKs/Left arm Ik Limb
    m_Weight: 1
  - m_Path: IKs/Right leg Ik Limb
    m_Weight: 1
  - m_Path: IKs/Left leg Ik Limb
    m_Weight: 1
  - m_Path: Controls
    m_Weight: 1
  - m_Path: Controls/Control Hips
    m_Weight: 1
  - m_Path: groundCheck
    m_Weight: 1
  - m_Path: flagInsert
    m_Weight: 1
