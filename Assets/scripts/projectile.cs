﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour {
	public int life;
	public float duration;
	public Vector2 myDamage;
	public AudioClip hitSound;
	public string myEffect;

	private float durationTimer;

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.transform.root.gameObject.layer == LayerMask.NameToLayer ("charCollision"))
			return;

		life--;
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		//damage enemies only
		if (collider.gameObject.layer == LayerMask.NameToLayer ("charCollision") && collider.gameObject.tag != tag) {
			
			health healthScript = (health)collider.transform.parent.GetComponent<health>();

			float randomDamge = Mathf.Round(Random.Range(myDamage.x, myDamage.y));

			//Debug.Log ("HIT!" + randomDamge);
			Debug.Log(healthScript);
			healthScript.modifyHealth (-randomDamge);


			damageIndicator dmg = (damageIndicator)Instantiate ((GameObject)Resources.Load("indicator"), transform.position - new Vector3(0, 0, 1), Quaternion.identity).GetComponent<damageIndicator>();
			dmg.SetText((-randomDamge).ToString());
			dmg.txtColor = Color.red;

			Effect ef = (Effect)Instantiate ((GameObject)Resources.Load(myEffect), transform.position - new Vector3(0, 0, 1), Quaternion.identity).GetComponent<Effect>();

			ef.myAudio.clip = hitSound;
			ef.myAudio.Play ();

			die ();
		}
	}

	void Update () {
		durationTimer += Time.deltaTime;

		if (durationTimer >= duration || life <= 0)
			die ();
	}

	void die()
	{
		Destroy (gameObject);
	}
}
