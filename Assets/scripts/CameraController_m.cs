﻿using UnityEngine;
using System.Collections;

public class CameraController_m : MonoBehaviour 
{
	public Transform Player;
	public float m_speed = 0.1f;
	Camera mycam;
	public SpriteRenderer spr;
	public float tempX, tempY;

	public void Start()
	{
		mycam = GetComponent<Camera> ();
	}

	public void Update()
	{

		//mycam.orthographicSize = (Screen.height / 100f) / 0.8f;

		if (Player) 
		{
		
			transform.position = Vector3.Lerp(transform.position, Player.position, m_speed) + new Vector3(0, 0, -12);
		}

		//Debug.Log (transform.position);
		tempX = ((spr.size.x/2) + transform.position.x);
		tempY = ((spr.size.y/2) + transform.position.y);
	}
}
