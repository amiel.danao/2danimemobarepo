﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour {

	public AudioSource myAudio;
	public float duration;

	private float durationTimer;

	
	// Update is called once per frame
	void Update () {
		durationTimer += Time.deltaTime;
		if (durationTimer >= duration)
			die ();
	}

	void die()
	{
		Destroy (gameObject);
	}
}
