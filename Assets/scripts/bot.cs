﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bot : MonoBehaviour {
	public Vector2 shootCD;
	public float realCD;

	public Vector2 jumpCD;
	public float realJumpCD;

	public float jumpForce;

	public GameObject bullet;
	public Transform handLeft;
	public Transform target;
	public LayerMask whatIsGround;
	public Rigidbody2D rb2d;

	private float shootCDTimer, jumpCDTimer;
	private bool canJump, isGrounded;

	// Use this for initialization
	void Start () {
		realCD = Random.Range(shootCD.x, shootCD.y);
	}
	
	// Update is called once per frame
	void Update () {

		//attack
		shootCDTimer += Time.deltaTime;

		if (shootCDTimer >= realCD) {
			Shoot ();
			shootCDTimer = 0;
			realCD = Random.Range(shootCD.x, shootCD.y);
		}

		//jump
		jumpCDTimer += Time.deltaTime;

		if (jumpCDTimer >= realJumpCD) {
			Jump ();
			jumpCDTimer = 0;
			realJumpCD = Random.Range(jumpCD.x, jumpCD.y);
			canJump = true;
		}

	}

	void Shoot()
	{
		Rigidbody2D shot = (Rigidbody2D)Instantiate(bullet, handLeft.position, Quaternion.identity).GetComponent<Rigidbody2D>();
		shot.velocity = (((target.position + new Vector3(0, 1, 0)) - transform.position).normalized * 16);
		shot.gameObject.tag = tag;
	}

	void Jump()
	{
		if (canJump && isGrounded) {
			rb2d.AddForce(new Vector2(0,jumpForce));
			canJump = false;
		}
	}

	void FixedUpdate()
	{
		isGrounded = Physics2D.OverlapCircle (transform.position - new Vector3(0, 1, 0), 0.2F, whatIsGround);
	}
}
