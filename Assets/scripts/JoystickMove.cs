using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UnityStandardAssets.CrossPlatformInput
{
	public class JoystickMove : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
	{
		public enum AxisOption
		{
			// Options for which axes to use
			Both, // Use both
			OnlyHorizontal, // Only horizontal
			OnlyVertical // Only vertical
		}

		public PlayerController_m character;
		public float lookSpeed = 5F;
		public int MovementRange = 100;
		public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
		public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
		public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

		public Vector3 m_StartPos;
		bool m_UseX; // Toggle for using the x axis
		bool m_UseY; // Toggle for using the Y axis
		CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
		CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input
		public Image JoystickImage;
		public Color aimColor, moveColor;
		public float angle;
		public Vector3 stickDirection;

		void Start()
		{
			m_StartPos = transform.position;
			stickDirection = transform.position - m_StartPos;
			CreateVirtualAxes();
		}

		void UpdateVirtualAxes(Vector3 value)
		{
			var delta = m_StartPos - value;
			delta.y = -delta.y;
			delta /= MovementRange;
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Update(-delta.x);
			}

			if (m_UseY)
			{
				m_VerticalVirtualAxis.Update(delta.y);
			}
		}

		void CreateVirtualAxes()
		{
			// set axes to use
			m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
			m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

			// create new axes based on axes to use
			if (m_UseX)
			{
				m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
			}
		}


		public void OnDrag(PointerEventData data)
		{
			Vector3 newPos = Vector3.zero;



			//			if (stickDist <= MovementRange / 2.5F || stickDist >= MovementRange / 1.1F) {
			if (m_UseX) {
				int delta = (int)(data.position.x - m_StartPos.x);
				//delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
				newPos.x = delta;
			}

			if (m_UseY) {
				int delta = (int)(data.position.y - m_StartPos.y);
				//delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
				newPos.y = delta;
			}
			//			}
			float stickDist = Vector3.Distance(m_StartPos, data.position);



			//Debug.Log (stickDist);

			//if (stickDist <= MovementRange / 2.5F)
			//transform.position = Vector3.ClampMagnitude (new Vector3 (newPos.x, newPos.y, newPos.z), MovementRange / 2.5F) + m_StartPos;


			float range = MovementRange/2.5F;

			if (stickDist >= MovementRange / 1.35F) {
				range = MovementRange;
				JoystickImage.color = moveColor;
			} else {
				JoystickImage.color = aimColor;
			}

			transform.position = Vector3.ClampMagnitude (new Vector3 (newPos.x, newPos.y, newPos.z), range) + m_StartPos;

			UpdateVirtualAxes (transform.position);

			//character aim
			//Vector2 direction = Camera.main.ScreenToWorldPoint(transform.position) - m_StartPos;
			stickDirection = transform.position - m_StartPos;
			angle = Mathf.Atan2 (stickDirection.y, stickDirection.x) * Mathf.Rad2Deg;
			//Quaternion rotation = Quaternion.AngleAxis (angle, Vector3.forward);
			float mirrorAngle = 0f;
			if (angle < 0f) {
				angle += 360f;
			}

			if (angle >= 90 && angle <= 270) {
				mirrorAngle = 180f;
			}

			if (angle >= 35 && angle <= 90)
				angle = 35;
			if (angle >= 90 && angle <= 145)
				angle = 145;

			if (angle >= 225 && angle <= 270)
				angle = 225;
			if (angle >= 270 && angle <= 360)
				angle = 315;


			//Debug.Log ("angle :  " + angle);

			//character.neck.eulerAngles = new Vector3 (0f, 0f, angle /*+ ((direction.x >= 0)? 0 : 180)*/);

			character.neck.localRotation = Quaternion.Euler(mirrorAngle, mirrorAngle,((angle >= 90 && angle <= 270)? Mathf.Abs(angle - 360) : angle));

			//character.neck.localScale = new Vector3(((direction.x >= 0)? 1 : -1), 1, 1);
			
			//character.neck.rotation = Quaternion.Slerp (character.neck.rotation, rotation, lookSpeed * Time.deltaTime);
		}

		public Vector3 getStickDirection()		{
			return stickDirection.normalized;//(transform.position - m_StartPos);
		}


		public void OnPointerUp(PointerEventData data)
		{
			stickDirection = transform.position - m_StartPos;
			transform.position = m_StartPos;
			UpdateVirtualAxes(m_StartPos);
			JoystickImage.color = aimColor;
		}


		public void OnPointerDown(PointerEventData data) { }

		void OnDisable()
		{
			// remove the joysticks from the cross platform input
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Remove();
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis.Remove();
			}
		}
	}
}