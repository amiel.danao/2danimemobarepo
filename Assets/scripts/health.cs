﻿using UnityEngine;
using UIHealthAlchemy;

public class health : MonoBehaviour {

	public float hp, maxHp, hpRegen;
	public RectHealhBar myHealthBar;
	private PlayerController_m myPlayerScript;
	public playerUI myUI;
	//private float regenTimer;

	void Start(){
		hp = maxHp;
		myPlayerScript = GetComponent<PlayerController_m> ();
	}

	// Update is called once per frame
	void Update () {
		if (hp <= 0)
			die ();

		//regen
		hp = Mathf.Clamp(hp + (hpRegen * Time.deltaTime), 0, maxHp);
		if (myHealthBar != null && hp != maxHp) {
			myHealthBar.value = (1 / maxHp) * hp;
			Color newColor = Color.HSVToRGB (Mathf.Clamp(((hp / maxHp) * 0.4F), 0, 1), 1, 0.86F);
			myHealthBar.realHealthBar.color = newColor;
			float alpha = 1-(hp/maxHp);
			Color newColor2 = new Color (1, 1, 1, alpha);
			myUI.criticalOverlay.color = newColor2;
		}
	}

	public void modifyHealth(float howMuch)
	{	

		hp = Mathf.Clamp(hp + howMuch, 0, maxHp);
		if (myHealthBar != null) {
			myHealthBar.value = (1 / maxHp) * hp;
			Color newColor = Color.HSVToRGB (Mathf.Clamp(((hp / maxHp) * 0.4F), 0, 1), 1, 0.86F);
			myHealthBar.realHealthBar.color = newColor;
			//Debug.Log (myHealthBar.value);
		}

		if (howMuch < 0) {
			if (myPlayerScript != null) {
				myPlayerScript.anim.SetTrigger ("blink");
				myUI.hurt ();
			}
		}

	}

	void die()
	{
		Destroy (gameObject);
	}
}
