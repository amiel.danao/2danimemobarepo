﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public int blueScore, redScore;
	public Text blueTxtScore, redTxtColor;
	public AudioSource myAudio;
	public AudioClip scoreSound;

	public void modifyScore(string teamColor, int howMuch)
	{
		if (teamColor == "Blue") {
			blueScore += howMuch;
		}

		if (teamColor == "Red") {
			redScore += howMuch;
		}

		updateScore ();
	}

	void updateScore()
	{
		blueTxtScore.text = "Blue : " + blueScore.ToString();
		redTxtColor.text = "Red : " + redScore.ToString();
		myAudio.PlayOneShot (scoreSound);
	}
}
