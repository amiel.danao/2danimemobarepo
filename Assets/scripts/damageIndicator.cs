﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class damageIndicator : MonoBehaviour {
	
	public float duration;

	public TextMesh txtmesh;
	public Color txtColor;
	private float alpha;

	private float durationTimer;

	void Start()
	{
		txtmesh.color = txtColor;
	}


	// Update is called once per frame
	void Update () {
		durationTimer += Time.deltaTime;
		if (durationTimer >= duration)
			die ();
		
		alpha -= Time.deltaTime;

		txtmesh.color = new Color(txtColor.r, txtColor.g, txtColor.b, alpha);
		transform.Translate (new Vector3(0, Time.deltaTime, 0));
	}

	public void SetText(string text){
		txtmesh.text = text;
		alpha = 1;
	}

	void die(){
		Destroy (gameObject);
	}
}
