﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class skill : MonoBehaviour {

	public JoystickMove myMoveJoyStick;
	public PlayerController_m myCharacter;
	public string keyMap;
	public float cooldown;
	public GameObject bullet;



	private bool canUse;
	private float cdTimer;


	// Use this for initialization
	void UseSkill () {
		Rigidbody2D shot = (Rigidbody2D)Instantiate(bullet, myCharacter.handRight.position, Quaternion.identity).GetComponent<Rigidbody2D>();
		shot.velocity = ((myMoveJoyStick.getStickDirection()) * 10);
		shot.gameObject.tag = tag;
	}
	
	// Update is called once per frame
	void Update () {
		if (cdTimer >= cooldown) {
			cdTimer = 0;
			canUse = true;
		} else {
			cdTimer += Time.deltaTime;
		}
		//shoot
		if (CrossPlatformInputManager.GetButton (keyMap) && canUse) {
			canUse = false;
			//anim.SetTrigger ("shoot");

			myCharacter.anim.SetTrigger ("shoot");
			//UseSkill ();
		}
	}
}
