using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UnityStandardAssets.CrossPlatformInput
{
	public class JoystickAim : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
	{
		public enum AxisOption
		{
			// Options for which axes to use
			Both, // Use both
			OnlyHorizontal, // Only horizontal
			OnlyVertical // Only vertical
		}

		public int MovementRange = 100;
		public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
		public string horizontalAxisName = "AxisX"; // The name given to the horizontal axis for the cross platform input
		public string verticalAxisName = "AxisY"; // The name given to the vertical axis for the cross platform input

		Vector3 m_StartPos;
		bool m_UseX; // Toggle for using the x axis
		bool m_UseY; // Toggle for using the Y axis
		CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
		CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input
		public Animator playerAnim;
		public Image JoystickImage;
		public Color aimColor, shootColor;


        void Start()
        {
            m_StartPos = transform.position;
			CreateVirtualAxes();
        }

		void UpdateVirtualAxes(Vector3 value)
		{
			var delta = m_StartPos - value;
			delta.y = -delta.y;
			delta /= MovementRange;
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Update(-delta.x);
			}

			if (m_UseY)
			{
				m_VerticalVirtualAxis.Update(delta.y);
			}
		}

		void CreateVirtualAxes()
		{
			// set axes to use
			m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
			m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

			// create new axes based on axes to use
			if (m_UseX)
			{
				m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
			}
		}


		public void OnDrag(PointerEventData data)
		{
			Vector3 newPos = Vector3.zero;



//			if (stickDist <= MovementRange / 2.5F || stickDist >= MovementRange / 1.1F) {
				if (m_UseX) {
					int delta = (int)(data.position.x - m_StartPos.x);
					//delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
					newPos.x = delta;
				}

				if (m_UseY) {
					int delta = (int)(data.position.y - m_StartPos.y);
					//delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
					newPos.y = delta;
				}
//			}
			float stickDist = Vector3.Distance(m_StartPos, data.position);



			//Debug.Log (stickDist);

				//if (stickDist <= MovementRange / 2.5F)
					//transform.position = Vector3.ClampMagnitude (new Vector3 (newPos.x, newPos.y, newPos.z), MovementRange / 2.5F) + m_StartPos;

					
				float range = MovementRange/2.5F;

				if (stickDist >= MovementRange / 1.05F) {
					range = MovementRange;
					playerAnim.SetBool ("shoot", true);
					JoystickImage.color = shootColor;
				} else {
					playerAnim.SetBool ("shoot", false);
					JoystickImage.color = aimColor;
				}

				transform.position = Vector3.ClampMagnitude (new Vector3 (newPos.x, newPos.y, newPos.z), range) + m_StartPos;
			
				UpdateVirtualAxes (transform.position);
			
		}


		public void OnPointerUp(PointerEventData data)
		{
			transform.position = m_StartPos;
			UpdateVirtualAxes(m_StartPos);
			JoystickImage.color = aimColor;
		}


		public void OnPointerDown(PointerEventData data) { }

		void OnDisable()
		{
			// remove the joysticks from the cross platform input
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Remove();
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis.Remove();
			}
		}
	}
}