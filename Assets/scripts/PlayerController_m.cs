﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
public class PlayerController_m : MonoBehaviour {
	
	public float maxSpeed = 6f;
	public float jumpForce = 1000f;
	public Transform groundCheck;
	public Transform neck;
	public LayerMask whatIsGround;
	public float verticalSpeed = 20;
	[HideInInspector]
	public bool lookingRight = true;
	bool doubleJump = false;
	private float doubleJumpAdd;
	private float deltaVertical;
	private bool m_isAxisInUse = false;
	public GameObject Boost;
	public GameObject Cloud;
	private Rigidbody2D rb2d;
	public Animator anim;
	private bool isGrounded = false;
	private int jumpNum;
	private float moveStickThreshold = 0.5F, jumpStickThreshold = 0.6F;
	public Transform handLeft, handRight;
	private string myTeamColor, enemyColor;
	public Score scoreScript;
	public Transform flagInsert;
	public AudioSource myAudio;
	public AudioClip doubleJumpSound;


	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		Cloud = GameObject.Find("Cloud");
		doubleJumpAdd = ((jumpForce/100) * 23);
		if (tag == "playerBlue") {
			myTeamColor = "Blue";
			enemyColor = "Red";
		}
		if (tag == "playerRed"){
			myTeamColor = "Red";
			enemyColor = "Blue";
		}
	}
	

	void OnCollisionEnter2D(Collision2D collision2D) {

		if (collision2D.relativeVelocity.magnitude > 20){
			Boost = Instantiate(Resources.Load("Prefabs/Cloud"), transform.position, transform.rotation) as GameObject;
		}
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		//try to score
		if (collider.tag == "base" + myTeamColor) {
			//return flag, if I have a flag
			flag flagScript = GetComponentInChildren<flag> ();

			if (flagScript) {
				flagScript.transform.parent = null;
				flagScript.player = null;
				flagScript.transform.position = flagScript.startPosition;
				flagScript.inUse = false;

				if (myTeamColor == "Blue") {
					scoreScript.modifyScore ("Blue", 1);
				}
				if (myTeamColor == "Red") {
					scoreScript.modifyScore ("Red", 1);
				}
			}
		}


		if (collider.tag == "teleport")
		{
			if(collider.name == "tp1")
				transform.position = GameObject.Find("tp2").transform.position + new Vector3(3, 0, 0);
			if(collider.name == "tp2")
				transform.position = GameObject.Find("tp1").transform.position + new Vector3(-3, 0, 0);
		}

		//put back flag if not in base
		if (collider.tag == "flag" + myTeamColor) {
			flag flagScript = collider.GetComponent<flag> ();
			if (flagScript.inUse && flagScript.player == null) {
				collider.transform.position = flagScript.startPosition;
				flagScript.inUse = false;
			}
		}

		//try to capture the flag
		if (collider.tag == "flag" + enemyColor){
			flag flagScript = collider.GetComponent<flag> ();
			if (flagScript.inUse == false && flagScript.player == null) {
				flagScript.inUse = true;
				flagScript.player = this;
				flagScript.transform.parent = flagInsert;
				flagScript.transform.localPosition = Vector3.zero;
			}
		}



		Debug.Log (collider.tag);
	}

	void Update () {
		//Debug.Log(CrossPlatformInputManager.GetAxisRaw("Vertical"));
		//Debug.Log(jumpNum);

		if (CrossPlatformInputManager.GetAxisRaw("Vertical") >= jumpStickThreshold && (m_isAxisInUse == false || jumpNum == 1) && (isGrounded || !doubleJump) )
		{
			if (!doubleJump && !isGrounded) {
				doubleJump = true;
				rb2d.AddForce(new Vector2(0,jumpForce + doubleJumpAdd));
				jumpNum = 2;
				myAudio.PlayOneShot (doubleJumpSound);
				//create jump smoke effect
				Instantiate(Resources.Load("effect3"), transform.position, Quaternion.identity);
				//Debug.Log ("second jump");
			} else 
			{
				rb2d.AddForce(new Vector2(0,jumpForce));
				jumpNum = 1;
			}
			m_isAxisInUse = true;
		}

		if (CrossPlatformInputManager.GetAxisRaw("Vertical") == 0)
			m_isAxisInUse = false;
			

	}


	void FixedUpdate()
	{
		if (isGrounded) {
			doubleJump = false;
			jumpNum = 0;
		}

		float hor = CrossPlatformInputManager.GetAxis ("Horizontal");
		float stopFactor = ((Mathf.Abs (hor) >= moveStickThreshold)? 1 : 0);


		anim.SetFloat ("Speed", Mathf.Abs (hor) * stopFactor);

		rb2d.velocity = new Vector2 (stopFactor * hor * maxSpeed, rb2d.velocity.y);		 


		isGrounded = Physics2D.OverlapCircle (groundCheck.position, 0.2F, whatIsGround);

		anim.SetBool ("IsGrounded", isGrounded);

		if ((hor > 0 && !lookingRight)||(hor < 0 && lookingRight))
			Flip ();
		 
		anim.SetFloat ("vSpeed", GetComponent<Rigidbody2D>().velocity.y);
	}

	
	public void Flip()
	{
		lookingRight = !lookingRight;
		/*Vector3 myScale = transform.localScale;
		myScale.x *= -1;
		transform.localScale = myScale;*/


		float myYRot = ((lookingRight == false)? 180: 0);
						
		transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, myYRot, transform.localEulerAngles.z);
		//flip the flag, if presenet
		if (flagInsert.childCount > 0)
			flagInsert.GetChild (0).transform.localRotation = Quaternion.Euler(0, myYRot, 90);
	}



}
