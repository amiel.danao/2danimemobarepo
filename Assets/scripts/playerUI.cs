﻿using UnityEngine;
using UnityEngine.UI;

public class playerUI : MonoBehaviour {

	public Image myProtrait;
	public Image criticalOverlay;
	public Sprite[] portraits;
	public health myHealth;

	public void hurt()
	{
		myProtrait.sprite = portraits [1];
		Invoke ("returnDefaultPortrait", 1.3f);
	}

	void Update()
	{
		//back to healthy portrait (regen)
		if ((myHealth.hp / myHealth.maxHp) >= 0.25) {
			if (myProtrait.sprite != portraits [0])
				myProtrait.sprite = portraits [0];
		}
	}
	
	void returnDefaultPortrait()
	{
		//low health
		if ((myHealth.hp / myHealth.maxHp) <= 0.25) {
			myProtrait.sprite = portraits [2];
		} else {
			myProtrait.sprite = portraits [0];
		}
	}
}